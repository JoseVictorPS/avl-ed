#ifndef AVL_H
#define AVL_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

typedef struct arv { // Nó de cada árvore
    int info; // Informação de cada nó
    struct arv * esq; // Ponteiro para o filho a esquerda
    struct arv * dir; // Ponteiro para o filho a direita
}AVL;

AVL * initialize();
AVL * gen_leaf(int c);
int empty_tree(AVL * a);
AVL * free_tree(AVL * a);
AVL* build_tree(AVL * r);
AVL * insert_tree(AVL * r, int v);
void print_tree(AVL*r);
void call_print_tree(AVL*r);
int read_value();
AVL*UX_tree();
int tree_height(AVL * r);
int max(int a, int b);
int balance_coef(AVL * r);
AVL * balance(AVL * r);
AVL * rse(AVL * r);
AVL * rsd(AVL * r);
AVL * rde(AVL * r);
AVL * rdd(AVL * r);
AVL * random_tree();
AVL * remove_node(AVL*r, int v);
AVL * free_node(AVL * r);
AVL * UX_remove(AVL*r);


AVL * initialize() {
   return NULL; // Retorna valor nulo para o ponteiro
}

AVL * gen_leaf(int c) {
    AVL * p = (AVL*)malloc(sizeof(AVL)); // Aloca nó de árvore
    p->info = c; //Preenche campo info do nó
    p->dir = NULL; // Filho da direita de folha é sempre vazio
    p->esq = NULL; // Filho da esquerda de folha é sempre vazio

    return p; // Retorna o nó
}

int empty_tree(AVL * a) { 
    return a==NULL; // Retorna se árvore está vazia
}

AVL * free_tree(AVL * a) { //É preciso ir até os filhos e depois liberar o nó
    if(!empty_tree(a)) { // Se árvore não está vazia
        free_tree(a->esq); // Vai até o filho da esquerda
        free_tree(a->dir); // Vai até o filho da direita
        free(a); // Libera o nó em questão
    }
    return NULL; // Atualiza o valor da árvore para nulo(vazia)
}



void print_tree(AVL*r) {
    printf("<");//Sinal de "menor que" significa abertura uma árvore/subárvore
    if(!empty_tree(r)) { // Se não estiver vazia
        printf(" %d ", r->info); // Imprime conteúdo do nó
        print_tree(r->esq); //Chama a função para a subárvore da esquerda
        print_tree(r->dir); //Chama a função para a subárvore da direita
    }
    printf("> "); // Fecha o conteúdo da árvore/subárvore
}

void call_print_tree(AVL*r) {
    print_tree(r);
    printf("\n");
}

AVL* build_tree(AVL * r) {
    int v = read_value(); // Lê um valor para inserção
    if(empty_tree(r)) { // Se a árvore/subárvore estiver vazia
        r = gen_leaf(v);//Cria uma folha
        return r;
    }
    return insert_tree(r, v); // Insere nos padrões da árvore de busca binária
    //PS: O elemento inserido sempre será inserido como folha. Ele será inserido
    //na subárvore da esquerda, caso seu campo info seja menor do que o da raíz atual
    //e na subárvore da direita, caso seja maior
}
AVL * insert_tree(AVL * r, int v) {
    if(empty_tree(r)) { // Se a árvore/subárvore estiver vazia
        r = gen_leaf(v);//Cria uma folha
        return r;
    }
    // Sempre retorna o nó filho chamado ao pai, durante a recursão, os nós já
    // existentes não são alterados, logo essa operação não altera a árvore
    // apenas quando se insere a folha, aí altera o pai da respectiva folha
    if(v > r->info) r->dir = insert_tree(r->dir, v);
    if(v < r->info) r->esq = insert_tree(r->esq, v);
    r = balance(r); // Balanceia a árvore
    return r; // Retorna o nó da chamada atual da recursão para o pai
}

int read_value() {
    printf("Qual o valor do nó que deseja inserir?  ");
    int v; // Inicializa valor a inserir no nó
    scanf("%d",&v); // Escaneia valor
    return v;
}

AVL*UX_tree() {
    int op; // Inicializa variável de interação com usuário
    AVL*raiz = initialize(); // Inicializa árvore binária
    
    do{ // Laços de preenchimento da árvore com interação com usuário
    raiz = build_tree(raiz); // Contrução da árvore nó a nó
    call_print_tree(raiz); // Imprime a árvore no momento atual

    printf("Deseja operar novamente?  ");
    scanf("%d", &op); // Escaneia se há mais nós para inserir
    }while(op);

    return raiz; // Retorna a árvore criada
}

int tree_height(AVL * r) {
    if(!r) return -1; //Caso a árvore seja vazia, altura dela é -1
    //Retorna maior altura entre subárvore esquerda e direita somada 1 unidade
    return max(tree_height(r->esq), tree_height(r->dir)) + 1;
}

int max(int a, int b) {
    return a > b ? a : b; //Pergunta se a é maior que b, se for returna a, se não, b
}

int balance_coef(AVL * r) {
    int esq = tree_height(r->esq); //Recebe altura da subárvore esquerda
    int dir = tree_height(r->dir); //Recebe altura da subárvore direita
    //A altura é calculada com as arestas até a folha mais distante, mas precisamos 
    //da aresta que liga à subárvore esquerda e direita, por isso incrementamos
    return ++esq - ++dir; //Coef de balanceamento é a diferençe das alturas
}

AVL * balance(AVL * r) {
    int b = balance_coef(r); //Recebe coef de bal. do nó em questão
    if(b==-2) {
        //Subárvore mais alongada e reta
        if(balance_coef(r->dir)==-1) r = rse(r); //Rotação simples à esquerda
        //Subárvore mais alongada e curva, precisa de duas rotações
        else if(balance_coef(r->dir)==1) r = rde(r);//Rotação dupla à esquerda
    }
    else if(b==2){
        //Subárvore mais alongada e reta
        if(balance_coef(r->esq)==1) r = rsd(r);//Rotação simples à direita
        //Subárvore mais alongada e curva, precisa de duas rotações
        else if(balance_coef(r->esq)==-1) r = rdd(r);//Rotação dupla à direita
    }
    //Repare que a curva acontece quando os sinais são diferentes
    return r;
}

AVL * rse(AVL * r) {
    AVL * ret = r->dir;//Salva nó diretamente à direita
    r->dir = ret->esq;//Salva subárvore da esquerda que será substituída
    ret->esq = r;//Substitui filho da esquerda pela antiga raíz
    return ret;
}

AVL * rsd(AVL * r) {
    AVL * ret = r->esq;//Salva nó diretamente à esquerda
    r->esq = ret->dir;//Salva subárvore da direita que será substituída
    ret->dir = r;//Substitui filho da direita pela antiga raíz
    return ret;
}

AVL * rde(AVL * r) {
    r->dir = rsd(r->dir);//Rotação simples à direita
    return rse(r);//Seguida se rotação simples à esquerda
}

AVL * rdd(AVL * r) {
    r->esq = rse(r->esq);//Rotação simples à esquerda
    return rsd(r);//Seguida se rotação simples à direita
}

AVL * random_tree() {
    AVL*raiz = initialize();//Inicializa árvore binária

    srand(time(NULL)); //Para prevenir repetição de sequencias entre execuções
    for(int i=0; i<12; i++)raiz=insert_tree(raiz, 1 + rand()%30);//Entre 1 e 30 inclusos

    call_print_tree(raiz);
    return raiz;
}

AVL * free_node(AVL * r) {
    if(r->esq == NULL && r->dir == NULL) { // Caso de nó folha
        free(r); // Libera o nó
        return NULL; // Retorna NULL para o ponteiro pai
    }
    if((r->esq&&!r->dir)||(!r->esq&&r->dir)) { // Caso filho úncio
        free(r); // Libera o nó
        if(r->esq) return r->esq; // Retorna filho único do nó removido para o pai
        else return r->dir; // para ocupar a mesma posição do nó removido
    // Casos em que há dois filhos
    }if(r->dir->esq) { /* Caso em que existe filho da esquerda, no filho da direita.
    É usado essa abordagem tendo em vista o uso de árvores binárias.
    O filho da esquerda é a subárvore onde tem elementos menores, seu filho da extrema
    esquerda representa o menor elemento dessa subárvore, mantendo o balanceamento*/
	    AVL * p = r->dir->esq; //Inicializa nó para substituir
	    while(p->esq) p=p->esq;//Enquanto houver filho à esquerda, há elementos menores
        r->info =p->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        /* Abaixo há a chamda recursiva, onde o menor elemento da subárvore direita
        pode ter no máximo um filho, assim essa próxima chamada recursiva é a última*/
        p = free_node(p); // Remove o nó redundante
    }
    else { // Caso em que o filho da esquerda não tem filho à direita
    /*Nesse caso o filho da direita substitui o pai, e é chamda a função para removê-lo
    Tendo em vista que ele não tem um filho na direita, senão teria entrado no caso
    acima, Na próxima chamada ele entrará no segundo caso(filho único) e o problema
    será resolvido*/
        r->info = r->dir->info; // Valor do nó removido é substituido
        // Assim há dois nós com o mesmo valor
        r->dir = free_node(r->dir); // Remove o nó redundante
    }
}

AVL * remove_node(AVL*r, int v) {
    if(empty_tree(r)) return NULL;//Se encontrar árvore vazia, retorna NULL
    //Se encontrar valor, o remove e retorna árvore balanceada
    if(r->info==v) {r= free_node(r); if(r)return balance(r);else return NULL;}
    if(r->info>v) r->esq=remove_node(r->esq, v);//Procura na subárvore esquerda
    if(r->info<v) r->dir=remove_node(r->dir, v);//Procura na subárvore direita
    return balance(r);//Atualiza a árvore a cada retorno da recursão
}
//r= free_node(r); if(r)return balance(r);return free_node(r);
AVL * UX_remove(AVL*r) {
    int op;
    int v;

    do {
        printf("Qual valor deseja retirar?\n");
        scanf("%d", &v);
        r = remove_node(r, v);

        call_print_tree(r);
        printf("Deseja retirar mais outro no?\n");
        scanf("%d", &op);
    }while(op);

    return r;
}

#endif //AVL_H