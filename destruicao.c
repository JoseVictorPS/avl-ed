#include "AVL.h"

int main(void) {
    AVL*raiz = UX_tree(); // Preenche a árvore com valores aleatórios
    raiz = UX_remove(raiz);

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}