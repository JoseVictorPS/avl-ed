#include "AVL.h"

int main(int argc, char *argv[]) {
    AVL*raiz = initialize(); // Inicializa ponteiro vazio
    raiz = UX_tree(); // Preenche a árvore por interação com usuario

    raiz = free_tree(raiz); // Atualiza para NULL raiz da árvore
    return 0;
}